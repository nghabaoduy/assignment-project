

import React, { Component } from 'react';
import {NativeModules} from 'react-native';
import {
   AppRegistry,
   Platform,
   StyleSheet,
   Text,
   View,
   FlatList,
   TouchableHighlight,
   Button
} from 'react-native';
import DiscoveryListItemView from '../views/DiscoveryListItemView';
import APIService from '../networks/APIService'

var dataManager = NativeModules.DataManager;

let apiService = new APIService();

export default class DiscoveryView extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
       isLoading: true,
       dataSource: [],
       page : 1,
       refreshing : false,
       error : null
    }
  };


  componentDidMount() {
    this.handleRemoteRequest();
  }

  //remote request from APISERVICE
  handleRemoteRequest = () => {
    let getDiscovery = apiService.getDiscoveryData(this.state.page);
    getDiscovery.then((responseJson) => {
      if (this.state.refreshing) {
        this.state.dataSource = [];
      }

      this.setState({
        dataSource: [...this.state.dataSource, ...responseJson.data],
        refreshing: false,
        page : responseJson.current_page,
        isLoading : false
      });
    })
    .catch((error) => {
      this.setState({
        dataSource: [],
        refreshing: false,
        page : 1,
        isLoading : false,
        error : error
      });
    });
  };

  //handling load more data when reach end of flat list
  handleLoadMore = () => {
    this.setState({
      page : this.state.page + 1,
    }, () => {
      this.handleRemoteRequest();
    });
  }

  //handling pull to refresh data
  handleRefresh = () => {

    this.setState({
      page : 1,
      refreshing :  true,
      error: null
    }, () => {
      this.handleRemoteRequest();
    });
  };


  _onPressItem = (item) => {
    if (this.state.refreshing) {
      console.log('page is refreshing');
      return;
    }
    dataManager.goToNextViewWithItem(item);
  };

  renderItem = ({item}) => (
      <DiscoveryListItemView
        onPressItem={this._onPressItem}
        itemData={item}
        bookingCount={item.booking_count}
        addressData={item.service_addresses}
        ratingStats={item.rating_overall_stats}
        brandName={item.brand_name}
        houseCall={item.housecall}
        houseCallCharge={item.housecall_charges_display}
        topServiceData={item.top_services}
        serviceCount={item.service_count}
        profileImage={item.image}
        displayImagesData={item.photos}
        isAwarded={item.vanitee_award_2016}
      />
  );


  render() {
    if (this.state.error) {
    //handling error when cannot get data
      return(
        <View style={styles.errorContainer}>
          <Text>Opps! Something is wrong</Text>
          <Button
            onPress={this.handleRefresh}
            title="RELOAD"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
          />
        </View>
      );
    }
    return (
       <FlatList style={styles.container}
         data={this.state.dataSource}
         renderItem={this.renderItem}
         keyExtractor={(item, index) => index}
         onRefresh={this.handleRefresh}
         refreshing={this.state.refreshing}
         onEndReached={this.handleLoadMore}
         onEndThreshold={0}
       />
    );
  };
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    marginTop: 0
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  errorContainer: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    marginTop: 64
  }
});