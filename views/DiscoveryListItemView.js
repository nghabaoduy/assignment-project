

import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity	
} from 'react-native';

//const value
let screenWidth = Dimensions.get('window').width;
const imageEmptyPlaceholder = require('../assets/no-image-thumbnail.png');
const imageLoveIcon = require('../assets/love-icon.png');
const imageAwardIcon =require('../assets/certificate.png');

//handling if case in render function
function renderIf(condition, content) {
    if (condition) {
        return content;
    } else {
        return null;
    }
}

export default class DiscoveryListItemView extends React.PureComponent {

	constructor(props) {
		super(props);
		this.state = {
		 isLoading: true
		};
	};

	_onPress = () => {
	    this.props.onPressItem(this.props.itemData);
	};

	// get booking count
	getBookingCount() {
		if (this.props.bookingCount != null) {
			return this.props.bookingCount + ' Bookings';
		}
		return 'Booking not found';
	}

	// get brand name
	getBrandName() {
		if (this.props.brandName != null) {
			return this.props.brandName;
		}
		return 'Brand name not found';
	}

	// get brand address, display houseCall if have
	getAddress() {
		if (this.props.addressData != null && this.props.addressData.length > 0 &&  this.props.addressData[0].address1 != null) {
			var houseCallInfo = '';
			if (this.props.houseCall != null && this.props.houseCall == true) {
				houseCallInfo = ' . Houses Call ($' + this.props.houseCallCharge + ')';
			}


			return this.props.addressData[0].address1 + houseCallInfo;
		}
		return 'Adress not found';
	}

	// get total rating of brand
	getRateStar() {
		if (this.props.ratingStats != null && this.props.ratingStats <= 100) {
			return (5.0 * this.props.ratingStats / 100.0).toFixed(1) + ' stars';
		}	
		return 'No rating'
	}

	// get service count
	getServiceCount() {
		if (this.props.serviceCount != null) {
			return '+' + this.props.serviceCount + ' more services';
		}
		return 'No extra service';
	}

	//get Top service infor with index, max index = 2
	getTopServiceInfoAtIndex(index) {
		if (index >=3 || this.props.topServiceData == null || this.props.topServiceData.length - 1 < index) {
			return 'Could not find top service';
		}

		let title = this.props.topServiceData[index].title;
		

		return title; 
	}

	//get Top service price with index, max index = 2
	getTopServicePriceAtIndex(index) {
		if (index >=3 || this.props.topServiceData == null || this.props.topServiceData.length - 1 < index) {
			return 'Could not find top service';
		}
		let price = this.props.topServiceData[index].price;
		let currency = this.props.topServiceData[index].currency; //TODO: to inplement if have currency involve
		return '$' + price;
	}

	//get display profile image
	getProfileImage() {
		if (this.props.profileImage != null) {
			return this.props.profileImage;
		}
		return 'https://i.stack.imgur.com/34AD2.jpg';
	}

	//get Display Image with Index, max index = 2, show placeholder image if no image data
	getDisplayImageAtIndex(index) {
		if (index >=3 || this.props.displayImagesData == null || this.props.displayImagesData.length - 1 < index) {
			return imageEmptyPlaceholder;
		}
		return {uri : this.props.displayImagesData[index].image};

	}

	getDisplayAward() {
		if (this.props.isAwarded != null && Boolean(this.props.isAwarded) == true) {
			return true;
		}
		return false;
	}


	//Rendering view
  	render() {
	    return (
	    	<TouchableOpacity onPress={this._onPress}>
	      	<View style={styles.mainView}>

	      		<View style={styles.mainContainer}>


	      			<View style={styles.gallery}>
				      	<Image
			              style={styles.photo}
			              source={this.getDisplayImageAtIndex(0)}
			            />
			            <Image
			              style={styles.photo}
			              source={this.getDisplayImageAtIndex(1)}
			            />
			            <Image
			              style={styles.photo}
			              source={this.getDisplayImageAtIndex(2)}
			            />
			        </View>

			        {renderIf(this.getDisplayAward(), 
		                <View style={styles.awardContainer}>
				        	<Image 
				        	style={styles.awardImage} 
				        	source={imageAwardIcon}
				        	/>

				        	<Text style={styles.awardText}>
					        	WINNER
				        	</Text>
				        </View>
		            )}

			        <View style={{flex: 0.8, flexDirection : 'row'}} hide ='true'>
						<View style={{flex: 1}}>
						</View>
						<View style={{flex: 2, flexDirection : 'row'}}>
							<View style={{flex: 4, flexDirection : 'column', justifyContent: 'center'}}>
								<Text style={styles.brandNameText}>
									{ this.getBrandName() }
								</Text>
								<Text style={styles.bookingCountText}>
									{ this.getBookingCount() }
								</Text>
								<Text style={styles.addressText}>
									{ this.getAddress() }
								</Text>
							</View>
							<View style={{flex: 1,justifyContent: 'center'}}>
								<Image style={{width: 40, height: 40, resizeMode: 'contain'}} source={imageLoveIcon}/>
							</View>
						</View>
					</View>

					<View style={{flex: 1, flexDirection : 'column', marginLeft: 20, marginRight: 20}}>
						<View style={styles.topServiceContainer}>
							<Text numberOfLines={1} style={{flex: 7}}>
								{this.getTopServiceInfoAtIndex(0)}
							</Text>
							<Text style={styles.topServicePriceText}>
								{this.getTopServicePriceAtIndex(0)}
							</Text>
						</View>
						<View style={styles.topServiceContainer}>
							<Text numberOfLines={1} style={{flex: 7}}>
								{this.getTopServiceInfoAtIndex(1)}
							</Text>
							<Text style={styles.topServicePriceText}>
								{this.getTopServicePriceAtIndex(1)}
							</Text>
						</View>
						<View style={styles.topServiceContainer}>
							<Text numberOfLines={1} style={{flex: 7}}>
								{this.getTopServiceInfoAtIndex(2)}
							</Text>
							<Text style={styles.topServicePriceText}>
								{this.getTopServicePriceAtIndex(2)}
							</Text>
						</View>
						<View style={styles.serviceCountContainer}>
							<Text style={styles.serviceCountText}>
								{ this.getServiceCount()}
							</Text>
						</View>
					</View>


					<View style={styles.imageView}>
						<View style={styles.imageContainer}>
							<Image
				              style={styles.image}
				              source={{uri: this.getProfileImage()}}
				            />
						</View>
					</View>


					<View style={styles.ratingView}>
						<View style={styles.ratingContainer}>
							<Text style={styles.ratingText}>
								{ this.getRateStar() }
							</Text>
						</View>
					</View>
	          	</View>
	        </View>
	        </TouchableOpacity>
	    );
  	};
}


const styles = StyleSheet.create({
  mainView: {
    width: '100%',
    height: 330,
  },
  mainContainer: {
  	flex: 1, 
  	flexDirection : 'column'
  },
  gallery: {
    width: '100%',
    height: screenWidth / 3,
    flexDirection : 'row',
  },
  infoView: {
    
  },
  photo: {
  	resizeMode: 'cover',
  	flex: 1,
  	width: screenWidth / 3,
  	height: screenWidth / 3
  },
  ratingView: {
  	width: 70, 
  	height: 20, 
  	marginTop : 190, 
  	marginLeft: 35, 
  	position: 'absolute'
  },
  ratingContainer: {
  	flex : 1, 
  	backgroundColor:'orange', 
  	borderRadius: 10,  
  	justifyContent: 'center'
  },
  ratingText: {
  	flex: 0.8,
  	textAlign: 'center', 
  	backgroundColor: 'rgba(0,0,0,0)',
  	fontSize: 13,
  	color: 'white'
  },
  imageView: {
  	width: 100, 
  	height: 100, 
  	marginTop : 100, 
  	marginLeft: 20, 
  	position: 'absolute',
  	shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 1,
    borderRadius: 50,
  },
  imageContainer: {
  	flex : 1, 
  	backgroundColor:'white', 
  	borderRadius: 50,  
  	justifyContent: 'center',
  	alignItems: 'center',
  },
  image: {
  	width: 96,
  	height: 96,
  	borderRadius: 48
  },
  addressText: {
  	fontSize : 12,
  	color: 'grey'
  },
  bookingCountText: {
  	fontSize : 12,
  	color: 'grey'
  },
  brandNameText: {
  	fontSize : 20
  },
  serviceCountContainer: {
  	flex: 1,
  	justifyContent: 'center'
  },
  serviceCountText: {
  	fontSize:  10,
  	color: 'grey',
  },
  topServiceText: {
  	fontSize: 12
  },
  topServicePriceText: {
  	fontSize: 12,
  	textAlign: 'right',
  	flex: 2
  },
  topServiceContainer: {
  	flex:1,
  	flexDirection : 'row',
  	borderBottomColor : 'grey',
  	borderBottomWidth : 1,
  	justifyContent: 'center',
  	alignItems: 'center',
  },
  awardContainer : {
  	backgroundColor: '#CCE15A',
  	justifyContent: 'center', 
  	width : 100, 
  	height : 20, 
  	position : 'absolute', 
  	marginTop : 20, 
  	right: 0, 
  	flexDirection : 'row'
  },
  awardImage: {
  	flex: 1, 
  	width: 20, 
  	height : 20, 
  	resizeMode: 'contain'
  },
  awardText: {
  	flex : 3, 
  	textAlign: 'center', 
  	marginRight : 5
  }
});