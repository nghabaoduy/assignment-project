import { AppRegistry } from 'react-native';
import App from './App';
import DiscoveryView from './views/DiscoveryView'


AppRegistry.registerComponent('AssignmentProject', () => App);
AppRegistry.registerComponent('DiscoveryView', () => DiscoveryView);
