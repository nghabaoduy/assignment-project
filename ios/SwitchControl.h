//
//  SwitchControl.h
//  AssignmentProject
//
//  Created by Yaga on 1/1/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "XibView.h"

@class SwitchControl;
@protocol SwitchControlDelegate<NSObject>
@optional
- (void)switchView:(SwitchControl *)switchControl selectedAtIndex:(int)index;

@end

@interface SwitchControl : XibView

@property (nonatomic, strong) NSArray<NSString *> *itemList;
@property (nonatomic) int currentIndex;
@property (nonatomic, strong) UIColor *itemNormalColor;
@property (nonatomic, strong) UIColor *itemHightlightColor;
@property (nonatomic, weak) id<SwitchControlDelegate> delegate;


/**
 Refresh switch to re-populate the data if needed. (Optional)
 */
- (void)refreshSwitchControl;

@end
