//
//  SwitchControl.m
//  AssignmentProject
//
//  Created by Yaga on 1/1/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "SwitchControl.h"
#import "SwitchControlItem.h"

@interface SwitchControl()
@property (weak, nonatomic) IBOutlet UIStackView *stackView;

@end

@implementation SwitchControl


- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self setUpXib];
    [self setUp];
  }
  return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self setUpXib];
    [self setUp];
  }
  return self;
}

#pragma mark - configuration

- (void)setUp {
  _currentIndex = 0;
  _itemList = @[@"First", @"Second"];
  [self refreshSwitchControl];
}

- (void)refreshSwitchControl {
  for (UIView *view in self.stackView.arrangedSubviews) {
    [self.stackView removeArrangedSubview:view];
  }
  

  for (int i = 0; i < self.itemList.count; i++) {
    NSString *item = self.itemList[i];
    SwitchControlItem *view =  [[SwitchControlItem alloc]init];
    view.title = item;
    if (self.itemNormalColor) {
      view.normalColor = self.itemNormalColor;
    }
    if (self.itemHightlightColor) {
      view.highlightedColor = self.itemHightlightColor;
    }
    [view setFrame:CGRectMake(0, 0, 100, 100)];
    [view.button addTarget:self action:@selector(itemAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.stackView addArrangedSubview:view];
    
    if (i == _currentIndex) {
      view.isSelected = YES;
    } else {
      view.isSelected = NO;
    }
  }
}

#pragma mark - getter and setter
- (void)itemAction:(UIButton *)sender {
  NSString *selectedText = sender.titleLabel.text;
  for (int i = 0; i < self.itemList.count; i ++) {
    if ([self.itemList[i] isEqualToString:selectedText] && i != _currentIndex) {
      self.currentIndex = i;
      return;
    }
  }
}

- (void)setCurrentIndex:(int)currentIndex {
  _currentIndex = currentIndex;
  for (int i = 0; i < self.itemList.count; i++) {
    SwitchControlItem *view =  self.stackView.arrangedSubviews[i];
    if (i == _currentIndex) {
      view.isSelected = YES;
    } else {
      view.isSelected = NO;
    }
  }
    if (self.delegate) {
        [self.delegate switchView:self selectedAtIndex:currentIndex];
    }
}

- (void)setItemList:(NSArray<NSString *> *)itemList {
  _itemList = itemList;
   [self refreshSwitchControl];
}


@end
