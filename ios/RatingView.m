//
//  RatingView.m
//  AssignmentProject
//
//  Created by Yaga on 31/12/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "RatingView.h"

#define STAR_FULL_ICON @"star-full"
#define STAR_OUTLINE_ICON @"star-outline"

@interface RatingView()

@property (weak, nonatomic) IBOutlet UIImageView *starFirst;
@property (weak, nonatomic) IBOutlet UIImageView *starSecond;
@property (weak, nonatomic) IBOutlet UIImageView *starThird;
@property (weak, nonatomic) IBOutlet UIImageView *starForth;
@property (weak, nonatomic) IBOutlet UIImageView *starFifth;

@end


@implementation RatingView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self setUpXib];
    [self setUp];
  }
  return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
     [self setUpXib];
    [self setUp];
  }
  return self;
}

- (void)setUp {
  _maxRate = 100;
  _currentRate = 0;
  [self refreshRatingView];
}

- (void)setCurrentRate:(float)currentRate {
  if (currentRate < 0) {
    return;
  }
  _currentRate = currentRate;
  [self refreshRatingView];
}

- (void)setMaxRate:(float)maxRate {
  if (maxRate < 0 || maxRate < self.currentRate) {
    return;
  }
  _maxRate = maxRate;
  [self refreshRatingView];
}


- (void)refreshRatingView {
  [self defaultRatingView];
  float result = self.currentRate / self.maxRate;
  
  if (result < 0.8) {
    self.starFifth.image = [UIImage imageNamed:STAR_OUTLINE_ICON];
  }
  if (result < 0.6) {
    self.starForth.image = [UIImage imageNamed:STAR_OUTLINE_ICON];
  }
  if (result < 0.4) {
    self.starThird.image = [UIImage imageNamed:STAR_OUTLINE_ICON];
  }
  if (result < 0.2) {
    self.starSecond.image = [UIImage imageNamed:STAR_OUTLINE_ICON];
  }
  if (result <= 0.0) {
    self.starFirst.image = [UIImage imageNamed:STAR_OUTLINE_ICON];
  }
}

- (void)defaultRatingView {
  self.starFirst.image = [UIImage imageNamed:STAR_FULL_ICON];
  self.starSecond.image = [UIImage imageNamed:STAR_FULL_ICON];
  self.starThird.image = [UIImage imageNamed:STAR_FULL_ICON];
  self.starForth.image = [UIImage imageNamed:STAR_FULL_ICON];
  self.starFifth.image = [UIImage imageNamed:STAR_FULL_ICON];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
