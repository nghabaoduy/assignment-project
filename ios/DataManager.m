//
//  DataManager.m
//  AssignmentProject
//
//  Created by Yaga on 31/12/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "DataManager.h"
#import "MProfessional.h"
#import "AppDelegate.h"
#import "DiscoveryViewController.h"

@implementation DataManager

RCT_EXPORT_MODULE();


RCT_EXPORT_METHOD(selectItemAtIndex:(int)index withData:(NSDictionary *)data) {
  NSLog(@"index %@ with data \n%@", @(index), data);
}


RCT_EXPORT_METHOD(goToNextViewWithItem:(NSDictionary *)data) {
  UIViewController *topViewController = [AppDelegate topMostController];
  
  //Checking whether topviewcontroller is DiscoveryViewController
  if ([topViewController isKindOfClass:[UINavigationController class]] && [((UINavigationController *)topViewController).topViewController isKindOfClass:[DiscoveryViewController class]]) {
    
    
    DiscoveryViewController *currentController = (DiscoveryViewController *)((UINavigationController *)topViewController).topViewController;
    
    //Parsing data from Dictionary to MProfessional
    MProfessional *professionalData = [[MProfessional alloc]initWithDictionary:data];
    
    //Command DiscoveryViewController to move to detailViewController
    [currentController moveToProfressionalDetailViewWithModel:professionalData];
  }
}

- (dispatch_queue_t)methodQueue
{
  return dispatch_get_main_queue();
}


@end
