//
//  AwardItemCollectionViewCell.m
//  AssignmentProject
//
//  Created by Yaga on 1/1/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "AwardItemCollectionViewCell.h"
@interface AwardItemCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

@end
@implementation AwardItemCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.containerView.layer.masksToBounds = NO;
    self.containerView.layer.cornerRadius = 3;
}
#pragma mark - getter & setter
- (void)setColor:(UIColor *)color {
    _color = color;
    self.containerView.layer.masksToBounds = YES;
    self.containerView.layer.cornerRadius = 6;
    self.containerView.backgroundColor = color;
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = _title;
}

- (void)setIcon:(UIImage *)icon {
  _icon = icon;
  self.iconImage.image = _icon;
}
@end



