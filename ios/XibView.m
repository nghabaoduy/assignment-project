//
//  XibView.m
//  AssignmentProject
//
//  Created by Yaga on 31/12/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "XibView.h"

@implementation XibView

- (XibView *)loadViewFromXib {
  NSString *className = NSStringFromClass([self class]);
  NSBundle *mainBundle= [NSBundle bundleWithIdentifier:@"com.assignmentproj"] ;
  NSArray *views = [mainBundle loadNibNamed:className owner:self options:nil];
  return views.firstObject;
}

- (void)setUpXib {
  _customView = [self loadViewFromXib];
  [self addSubview:_customView];
  
  _customView.frame = self.bounds;
  //_customView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  _customView.translatesAutoresizingMaskIntoConstraints = NO;
  NSDictionary *views = @{
                          @"childView" : _customView
                          };
  
  [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[childView]-0-|" options:0 metrics:nil views:views]];
  [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[childView]-0-|" options:0 metrics:nil views:views]];
  //[self setNeedsLayout];
  //[self layoutIfNeeded];
}

- (void)layoutSubviews {
  [super layoutSubviews];
  [_customView layoutIfNeeded];
}

@end
