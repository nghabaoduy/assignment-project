//
//  MProfessional.h
//  AssignmentProject
//
//  Created by Yaga on 31/12/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MProfessional : NSObject

@property (nonatomic, strong) NSString *brandName;
@property (nonatomic, strong) NSString *pDescription;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *displayPhotoURLString;
@property (nonatomic, strong) NSArray<NSString *> *galleryPhotosURL;
@property (nonatomic) float rating;// max rating = 100
@property (nonatomic) int reviewCount;
@property (nonatomic) int bookingCount;
@property (nonatomic) int photoCount;
@property (nonatomic) double houseCallPrice;
@property (nonatomic) BOOL isHouseCall; // if house call is true then we can use house call Price
@property (nonatomic) BOOL isVaniteeAward2016; // only for award tag

- (instancetype)initWithDictionary:(NSDictionary *)data;

@end
