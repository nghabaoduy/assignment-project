//
//  MProfessional.m
//  AssignmentProject
//
//  Created by Yaga on 31/12/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "MProfessional.h"

@implementation MProfessional
- (instancetype)init {
  self = [super init];
  if (self) {
    _brandName = @"No brand name";
    _pDescription = @"No Description";
    _fullName = @"No name";
    _displayPhotoURLString = @"";
    _address = @"No address";
    _galleryPhotosURL = @[];
    _rating = 0.0f;
    _reviewCount = 0;
    _bookingCount = 0;
    _photoCount = 0;
    _houseCallPrice = 0.0;
    _isHouseCall = NO;
    _isVaniteeAward2016 = NO;
  }
  return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)data {
  self = [self init];
  if (self) {
    
    if (data != nil) {
      if (data[@"brand_name"] != nil && [data[@"brand_name"] isKindOfClass:[NSString class]]) {
        _brandName = data[@"brand_name"];
      }
      
      if (data[@"description"] != nil && [data[@"description"] isKindOfClass:[NSString class]]) {
        _pDescription = data[@"description"];
      }
      
      if (data[@"user"] != nil && [data[@"user"] isKindOfClass:[NSDictionary class]]) {
        NSString *lastName = @"";
        if (data[@"user"][@"last_name"] != nil && [data[@"user"][@"last_name"] isKindOfClass:[NSString class]]) {
          lastName = data[@"user"][@"last_name"];
        }
        
        NSString *firstName = @"";
        if (data[@"user"][@"first_name"] != nil && [data[@"user"][@"first_name"] isKindOfClass:[NSString class]]) {
          firstName = data[@"user"][@"first_name"];
        }
        
        _fullName = [NSString stringWithFormat:@"%@ %@", lastName, firstName];
      }
      
      
      if (data[@"rating_overall_stats"] != nil && [data[@"rating_overall_stats"] isKindOfClass:[NSNumber class]]) {
        _rating = [data[@"rating_overall_stats"] floatValue];
      }
      
      if (data[@"review_count"] != nil && [data[@"review_count"] isKindOfClass:[NSNumber class]]) {
        _reviewCount = [data[@"review_count"] intValue];
      }
      
      if (data[@"booking_count"] != nil && [data[@"booking_count"] isKindOfClass:[NSNumber class]]) {
        _bookingCount = [data[@"booking_count"] intValue];
      }
      
      if (data[@"housecall"] != nil && [data[@"housecall"] isKindOfClass:[NSNumber class]]) {
        _isHouseCall = [data[@"housecall"] boolValue];
      }
      
      if (data[@"housecall_charges_display"] != nil && [data[@"housecall_charges_display"] isKindOfClass:[NSNumber class]]) {
        _houseCallPrice = [data[@"housecall_charges_display"] doubleValue];
      }
      
      if (data[@"vanitee_award_2016"] != nil && [data[@"vanitee_award_2016"] isKindOfClass:[NSNumber class]]) {
        _isVaniteeAward2016 = [data[@"vanitee_award_2016"] boolValue];
      }
      
      if (data[@"service_addresses"] != nil && [data[@"service_addresses"] isKindOfClass:[NSArray class]]) {
        //NSLog(@"service address %@", data[@"service_addresses"][0]);
        if ([data[@"service_addresses"] count] >= 1 && data[@"service_addresses"][0][@"address1"] != nil && [data[@"service_addresses"][0][@"address1"] isKindOfClass:[NSString class]]) {
          _address = data[@"service_addresses"][0][@"address1"];
        }
        
      }
      
      if (data[@"image"] != nil && [data[@"image"] isKindOfClass:[NSString class]]) {
        _displayPhotoURLString = data[@"image"];
      }
      
      //Currently dont have all related photos so take display photo value instad
      //TODO: change to photo list later
      if (data[@"photos"] != nil && [data[@"photos"] isKindOfClass:[NSArray class]]) {
        NSMutableArray *temp = [NSMutableArray new];
        for (NSDictionary *photoData in data[@"photos"]) {
          if (photoData[@"image"] != nil && [photoData[@"image"] isKindOfClass:[NSString class]]) {
            [temp addObject:photoData[@"image"]];
          }
        }
        _galleryPhotosURL = temp.copy;
      }
      
    } else {
      NSLog(@"Professional data is nil");
    }
  }
  return self;
}

- (NSString *)description {
  return [NSString stringWithFormat:@"> brand name : %@ \ndescription: %@\nFull name: %@", _brandName, _pDescription, _fullName];
}

#pragma mark - setter and getter
- (void)setRating:(float)rating {
  if (rating < 0) {
    rating = 0;
  }
  else if (rating > 100) {
    rating = 100;
  }
  _rating = rating;
}

- (void)setReviewCount:(int)reviewCount {
  if (reviewCount < 0) {
    reviewCount = 0;
  }
  _reviewCount = reviewCount;
}

@end
