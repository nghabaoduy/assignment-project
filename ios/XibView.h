//
//  XibView.h
//  AssignmentProject
//
//  Created by Yaga on 31/12/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XibView : UIView

@property (nonatomic, strong) XibView *customView;

- (void)setUpXib;

@end
