//
//  RatingView.h
//  AssignmentProject
//
//  Created by Yaga on 31/12/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "XibView.h"


@interface RatingView : XibView

@property (nonatomic) float currentRate;
@property (nonatomic) float maxRate; //default = 100

/**
 Refreshing current Rating View. It will re-caculate currentRate and maxRate.
 */
- (void)refreshRatingView;

@end
