//
//  DiscoveryViewController.h
//  AssignmentProject
//
//  Created by Yaga on 27/12/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MProfessional.h"

@interface DiscoveryViewController : UIViewController

- (void)moveToProfressionalDetailViewWithModel:(MProfessional *)data;

@end
