//
//  DiscoveryViewController.m
//  AssignmentProject
//
//  Created by Yaga on 27/12/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "DiscoveryViewController.h"
#import "AppDelegate.h"
#import <React/RCTRootView.h>
#import <React/RCTBundleURLProvider.h>
#import "ProfessionalDetailViewController.h"

@interface DiscoveryViewController ()

@property (nonatomic, strong) RCTRootView *rootView;

@end

@implementation DiscoveryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"Discovery";
    NSURL *jsCodeLocation = [NSURL URLWithString:@"http://localhost:8081/index.bundle?platform=ios"];

    jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
    self.rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation moduleName:@"DiscoveryView" initialProperties:nil launchOptions:nil];
    //self.rootView.sizeFlexibility = RCTRootViewSizeFlexibilityWidthAndHeight;
    self.rootView.frame = [UIScreen mainScreen].bounds;
    [self.view addSubview:self.rootView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
  
  UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
  float additionalHeight = 0.0;
  if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
    additionalHeight = self.navigationController.navigationBar.frame.size.height + 20;
  }
  else if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
    additionalHeight = self.navigationController.navigationBar.frame.size.height;
  }
  else {
    additionalHeight = 64;
  }
  
  self.rootView.frame = CGRectMake(0, additionalHeight, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
}

- (void)moveToProfressionalDetailViewWithModel:(MProfessional *)data {
  ProfessionalDetailViewController *destination = [[ProfessionalDetailViewController alloc] initWithProfessionalData:data];
  [self.navigationController pushViewController:destination animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
