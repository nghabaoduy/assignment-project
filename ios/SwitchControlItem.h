//
//  SwitchControlItem.h
//  AssignmentProject
//
//  Created by Yaga on 1/1/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "XibView.h"

@interface SwitchControlItem : XibView

@property (nonatomic, strong) NSString *title; //default is Item
@property (nonatomic, strong) UIColor *normalColor; //default is gray
@property (nonatomic, strong) UIColor *highlightedColor; //default is red
@property (nonatomic) BOOL isSelected; //default is NO
@property (nonatomic, readonly) UIButton *button;

@end
