//
//  ProfessionalDetailViewController.h
//  AssignmentProject
//
//  Created by Yaga on 31/12/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MProfessional.h"

@interface ProfessionalDetailViewController : UIViewController

- (instancetype)initWithProfessionalData:(MProfessional *)data;

@end
