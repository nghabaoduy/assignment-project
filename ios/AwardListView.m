//
//  AwardListView.m
//  AssignmentProject
//
//  Created by Yaga on 1/1/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "AwardListView.h"
#import "AwardItemCollectionViewCell.h"

@implementation AwardItem
@end


@interface AwardListView() <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *noAwardLabel;

@end

@implementation AwardListView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self setUpXib];
    [self setUp];
  }
  return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self setUpXib];
    [self setUp];
  }
  return self;
}

#pragma mark - configuration
- (void)setUp {
  //collection view
  UINib *awardCell = [UINib nibWithNibName:@"AwardItemCollectionViewCell" bundle:nil];
  [self.collectionView registerNib:awardCell forCellWithReuseIdentifier:@"cell"];
  self.collectionView.dataSource = self;
  self.collectionView.delegate = self;
  [self.collectionView reloadData];
  self.noAwardLabel.hidden = NO;

}

#pragma mark - setter and getter
- (void)setItems:(NSArray<AwardItem *> *)items {
  if (items.count > 0) {
    self.noAwardLabel.hidden = YES;
  }
  _items = items;
}

#pragma mark - delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  AwardItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
  AwardItem *item = self.items[indexPath.row];
  cell.color = item.color;
  cell.title = item.title;
  if (item.icon != nil) {
    cell.icon = item.icon;
  }
  
  return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  return CGSizeMake(80, self.bounds.size.height);
}

@end
