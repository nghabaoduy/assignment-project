//
//  AwardListView.h
//  AssignmentProject
//
//  Created by Yaga on 1/1/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "XibView.h"

@interface AwardItem : NSObject


@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) UIImage *icon;

@end

@interface AwardListView : XibView

@property (nonatomic, strong) NSArray <AwardItem *> *items;

@end
