//
//  AwardItemCollectionViewCell.h
//  AssignmentProject
//
//  Created by Yaga on 1/1/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AwardItemCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIImage *icon;

@end
