//
//  SwitchControlItem.m
//  AssignmentProject
//
//  Created by Yaga on 1/1/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "SwitchControlItem.h"

@interface SwitchControlItem()

@property (weak, nonatomic) IBOutlet UIButton *itemButton;
@property (weak, nonatomic) IBOutlet UIView *borderBottomView;

@end

@implementation SwitchControlItem

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self setUpXib];
    [self setUp];
  }
  return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self setUpXib];
    [self setUp];
  }
  return self;
}

#pragma mark - configuration
- (void)setUp {
    _title = @"Item";
    _normalColor = [UIColor grayColor];
    _highlightedColor = [UIColor redColor];
    self.borderBottomView.backgroundColor = _highlightedColor;
    [self.itemButton setTitle:self.title forState:UIControlStateNormal];
}

#pragma mark - setter and getter

- (void)setTitle:(NSString *)title {
    _title = title;
    [self.itemButton setTitle:_title forState:UIControlStateNormal];
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    if (_isSelected) {
        [self.itemButton setTitleColor:self.highlightedColor forState:UIControlStateNormal];
        self.borderBottomView.hidden = NO;
    } else {
        [self.itemButton setTitleColor:self.normalColor forState:UIControlStateNormal];
      self.borderBottomView.hidden = YES;
    }
}

- (void)setHighlightedColor:(UIColor *)highlightedColor {
    _highlightedColor = highlightedColor;
    self.borderBottomView.backgroundColor = _highlightedColor;
}

- (void)setNormalColor:(UIColor *)normalColor {
    _normalColor = normalColor;
}

- (UIButton *)button {
    return self.itemButton;
}

@end
