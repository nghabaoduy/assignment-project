//
//  ImageGalleryCollectionViewCell.h
//  AssignmentProject
//
//  Created by Yaga on 1/1/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageGalleryCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
