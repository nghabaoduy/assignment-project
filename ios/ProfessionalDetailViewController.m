//
//  ProfessionalDetailViewController.m
//  AssignmentProject
//
//  Created by Yaga on 31/12/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "ProfessionalDetailViewController.h"
#import "RatingView.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "SwitchControl.h"
#import "ImageGalleryCollectionViewCell.h"
#import "AwardListView.h"

#define LOVE_ICON @"love-icon"
#define SHARE_ICON @"share"

@interface ProfessionalDetailViewController () <SwitchControlDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet RatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *ratingAndBookingLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressAndHouseCallLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet SwitchControl *switchControl;
@property (weak, nonatomic) IBOutlet UIView *containerView;
//info subview
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;

//gallery subview
@property (weak, nonatomic) IBOutlet UIView *galleryView;
@property (weak, nonatomic) IBOutlet UICollectionView *galleryCV;

//not found subview
@property (weak, nonatomic) IBOutlet UIView *notFoundView;
@property (weak, nonatomic) IBOutlet AwardListView *awardListView;

@property (nonatomic, strong) MProfessional *curProfessionalData;

@end

@implementation ProfessionalDetailViewController
- (instancetype)initWithProfessionalData:(MProfessional *)data {
  self = [super init];
  if (self) {
    _curProfessionalData = data;
  }
  return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self configUI];
  
    // navigation title
    self.title = _curProfessionalData.brandName;
    
    self.ratingView.currentRate = self.curProfessionalData.rating;
    self.ratingAndBookingLabel.text = [self getRatingAndBooking];
    self.addressAndHouseCallLabel.text = [self getAddressAndHouseCall];
    self.infoTextView.text = self.curProfessionalData.pDescription;
    self.awardListView.items = [self getAwardItemList];
  
  
    //handling loading image from URL & caching with afnetworking
    [self.profileImageView setImageWithURL:[NSURL URLWithString:self.curProfessionalData.displayPhotoURLString] placeholderImage:nil];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


#pragma mark - configuration
- (void)configUI {
  //profile image view
  self.profileImageView.layer.masksToBounds = YES;
  self.profileImageView.layer.cornerRadius = 45;
  
  //switch Control
  [self.switchControl setItemList:@[@"INFO", @"GALLERY", @"SERVICE", @"REVIEW"]];
  self.switchControl.delegate = self;
  
  //gallery collection
  self.galleryCV.delegate = self;
  self.galleryCV.dataSource = self;
  UINib *galleryCell = [UINib nibWithNibName:@"ImageGalleryCollectionViewCell" bundle:nil];
  [self.galleryCV registerNib:galleryCell forCellWithReuseIdentifier:@"cell"];
  [self.galleryCV reloadData];
  
  
  //container subview
  self.infoTextView.hidden = NO;
  self.galleryView.hidden = YES;
  self.notFoundView.hidden = YES;
  
  //love and share icon
  UIBarButtonItem *loveIcon = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:LOVE_ICON] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(loveAction:)];
  UIBarButtonItem *shareIcon = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:SHARE_ICON] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(shareAction:)];
  self.navigationItem.rightBarButtonItems = @[shareIcon, loveIcon];
}




#pragma mark - getter and setter
- (NSString *)getRatingAndBooking {
    return [NSString stringWithFormat:@"(%d) | %d Bookings", self.curProfessionalData.reviewCount, self.curProfessionalData.bookingCount];
}

- (NSString *)getAddressAndHouseCall {
    if (!self.curProfessionalData.isHouseCall) {
        return [NSString stringWithFormat:@"%@", self.curProfessionalData.address];
    }
    return [NSString stringWithFormat:@"%@ . Houses Call ($%f.2)", self.curProfessionalData.address, self.curProfessionalData.houseCallPrice];
}

- (NSArray <AwardItem *> *)getAwardItemList {
  if (self.curProfessionalData.isVaniteeAward2016) {
    AwardItem *award2016 = [AwardItem new];
    award2016.title = @"WINNER";
    award2016.color = [UIColor greenColor];
    return @[award2016];
  }
  return nil;
}

#pragma mark - action
- (void)loveAction:(id)sender {
  //TODO: implement later
}

- (void)shareAction:(id)sender {
  //TODO: implement later
}

- (IBAction)callAction:(id)sender {
  //TODO: implement later
}

- (IBAction)messageAction:(id)sender {
  //TODO: implement later
}


#pragma mark - delegate
- (void)switchView:(SwitchControl *)switchControl selectedAtIndex:(int)index {
    self.infoTextView.hidden = YES;
    self.galleryView.hidden = YES;
    self.notFoundView.hidden = YES;
    
    if (index == 0) {
        self.infoTextView.hidden = NO;
    }
    else if (index == 1) {
        self.galleryView.hidden = NO;
      [self.galleryCV reloadData];
    }
    else {
        self.notFoundView.hidden = NO;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return self.curProfessionalData.galleryPhotosURL.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  
  ImageGalleryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
  
  NSString *urlString = self.curProfessionalData.galleryPhotosURL[indexPath.row];
  [cell.imageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:nil];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  return CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
}




@end
