import React from 'react';

export default class APIService {

	// deprecated
	getDataList() {
		return fetch('https://staging.vanitee.com/api/professionals/search?access_token=BuB58yrvKIX7gsapvxx4qscxUctao0xm5DVcV5wO&status_filter=published&order=desc&results=10&page=1')
		.then((response) => response.json());
	};


	//get all discovery data with pagination
	getDiscoveryData(pageIndex = 1, pageSize = 10) {
		return fetch('https://staging.vanitee.com/api/professionals/search?access_token=BuB58yrvKIX7gsapvxx4qscxUctao0xm5DVcV5wO&status_filter=published&order=desc&results='+pageSize +'&page=' + pageIndex)
		.then((response) => response.json());
	}

	//for testing purpose
	getErrorDiscoveryData(pageIndex = 1, pageSize = 10) {
		return fetch('https://stagi.vanitee.com/api/professionals/search?access_token=BuB58yrvKIX7gsapvxx4qscxUctao0xm5DVcV5wO&status_filter=published&order=desc&results='+pageSize +'&page=' + pageIndex)
		.then((response) => response.json());
	}
}	